package eu.epitech.java;

import com.google.common.base.Predicate;

/**
 * predicate used to pick up Even Number from an interable
 * 
 * @author nicolas
 * 
 */
final public class EvenNumberSelector implements Predicate<Integer> {
	public boolean apply(Integer arg0) {
		return (arg0 % 2 == 0);
	}
}