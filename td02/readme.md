# TD2 : MVC et Spring Le ReToUr

## Objectifs du TD

### Rappels
+ Récap du TD00, TD0, TD01
+ MVC

### Techniques
+ repartir du td02-handson (version annotée) 

## TD02


### Travail collaborarif pendant le TD (rappel du TD01)
+ notre base commune de code: java101-epitech
+ créez votre compte bitbucket
+ allez sur la page du projet java101-epitech ( https://bitbucket.org/mirlitone/java101-epitech)
+ cliquez sur Fork
+ clonnez votre nouveau repository en local
+ editez les fichier dans td02/td02-handson
+ pusher dans votre repository forké
+ demander une pull request de votre repository (master) vers le repository  mirlitone/java101-epitech (master) et attendez le résultat de la code review


### Rappels des besoin

* 1 cours = 1 prof + 1 horaire
* un cours est fait pendant une plage horaire (toute les semaines) (eg. JAVA tous les lundi de 15h à 18h)
* deux cours ne peuvent pas se chevaucher
* lors de la création d'un nouveau cours, système doit proposer 3 horaires disponibles sur la semaine dans la tranche des Lundi-Vendredi ; 9h-12h30 14h-18h. Si aucun des 3 horaires ne convient, le système doit afficher 3 autres horaires et ainsi de suite.

* Pages à créer: 
* /cours qui affiche la liste des cours avec leurs horaires et les prof associés
* /profs qui affiche la liste des prof avec leurs cours associés. Il affiche l'emploi du temps d'un prof
* /nouveau-cours qui affiche un formulaire qui permet noter le nom du cours, le prof associé (dans la liste des profs), et qui propose de choisir l'horaire du cours dans une liste.







